.. index::
   single: Function reference; Tools

Additional functions
====================

.. automodule:: calorine.tools
   :members:
