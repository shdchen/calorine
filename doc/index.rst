:program:`calorine`
*******************

:program:`calorine` is a Python library for constructing and sampling neuroevolution potentials (NEPs) via `GPUMD <https://gpumd.zheyongfan.org/>`_.
It provides ASE calculators, IO functions for reading and writing :program:`GPUMD` input and output files, as well as a Python interface that allows inspection NEP models.


.. toctree::
   :maxdepth: 2
   :caption: Main

   installation

.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   tutorials/calculators
   tutorials/nep_descriptors
   tutorials/structure_relaxation

.. toctree::
   :maxdepth: 2
   :caption: Function reference

   calculators
   gpumd
   nep
   tools

.. toctree::
   :maxdepth: 2
   :caption: Backmatter

   genindex
