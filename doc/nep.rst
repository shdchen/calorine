.. index::
   single: Function reference; NEP interface

NEP interface
=============

.. automodule:: calorine.nep
   :members:
