This directory contains the source files (`nepy.*`) for building the pybind interface for the `nepy` module.
The CPU only implementation of the NEP was copied from `https://github.com/brucefan1983/NEP_CPU.git` (v1.1, ed39bbb4c8db8).
