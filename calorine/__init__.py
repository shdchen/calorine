# -*- coding: utf-8 -*-
"""
Main module of the calorine package.
"""

__project__ = 'calorine'
__description__ = 'A Python library for building and sampling NEP models via GPUMD simulations.'
__copyright__ = '2022'
__license__ = 'Mozilla Public License 2.0 (MPL 2.0)'
__version__ = '1.3'
__maintainer__ = 'The calorine developers team'
__status__ = 'Stable'
__url__ = 'http://calorine.materialsmodeling.org/'
