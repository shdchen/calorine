from ase import Atoms
from ase.constraints import ExpCellFilter
from ase.optimize import BFGS, FIRE


def relax_structure(structure: Atoms,
                    max_force: float = 0.001,
                    max_n_steps: int = 200,
                    minimizer: str = 'bfgs',
                    constant_cell: bool = False,
                    constant_volume: bool = False) -> None:
    """Relaxes the given structure.

    Parameters
    ----------
    structure
        atomic configuration to relax
    max_force
        if the absolute force for all atoms falls below this value the relaxation is stopped
    max_n_steps
        maximum number of relaxation steps the minimizer is allowed to take
    minimizer
        minimizer to use; possible values: 'fire', 'bfgs'
    constant_cell
        if True do not relax the cell or the volume
    constant_volume
        if True relax the cell shape but keep the volume constant
    """
    if structure.calc is None:
        raise ValueError('Structure has no attached calculator object')
    if constant_cell:
        ucf = structure
    else:
        ucf = ExpCellFilter(structure, constant_volume=constant_volume)
    if minimizer == 'fire':
        dyn = FIRE(ucf, logfile=None)
        dyn.run(fmax=max_force, steps=max_n_steps)
    elif minimizer == 'bfgs':
        dyn = BFGS(ucf, logfile=None)
        dyn.run(fmax=max_force, steps=max_n_steps)
    else:
        raise ValueError(f'Unknown minimizer: {minimizer}')
