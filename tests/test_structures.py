import numpy as np
import pytest
from ase import Atoms
from ase.build import bulk
from calorine.tools import relax_structure
from calorine.calculators import CPUNEP


@pytest.fixture
def PbTe() -> Atoms:
    """ASE Atoms object compatible with calculator."""
    structure = bulk('PbTe', crystalstructure='rocksalt', a=4)
    structure[0].position += np.array([0.03, 0.08, 0])
    return structure


@pytest.fixture
def PbTeWithCalc(PbTe: Atoms) -> Atoms:
    structure = PbTe.copy()
    nep3 = 'tests/nep_models/nep3_v3.3.1_PbTe_Fan22.txt'
    structure.calc = CPUNEP(nep3)
    return structure


@pytest.mark.parametrize('minimizer, constant_cell, constant_volume, max_force, volume, cell', [
    (
        'bfgs',
        False,
        False,
        0.0340827,
        19.4958,
        [[0.208096, 2.12256, 2.18454],
         [2.12267, 0.208096, 2.18454],
         [2.33077, 2.33066, 4.29182e-15]]
    ),
    (
        'bfgs',
        False,
        True,
        0.0208667,
        15.9999,
        [[0.513651, 2.04617, 2.03926],
         [2.04617, 0.513651, 2.03926],
         [2.55982, 2.55982, 1.16682e-14]]
    ),
    (
        'fire',
        True,
        False,  # Volume is fixed if cell is fixed
        0.0345745,
        15.9999,
        [[0.0, 2.0, 2.0],
         [2.0, 0.0, 2.0],
         [2.0, 2.0, 0.0]]
    ),
])
def test_check_max_force(PbTeWithCalc,
                         minimizer,
                         constant_cell,
                         constant_volume,
                         max_force,
                         volume, cell):
    """Tests that a structure is relaxed to a maximum force lower than the specified threshold."""
    pos_pre_relax = PbTeWithCalc.get_positions()
    forces_pre_relax = PbTeWithCalc.get_forces()
    max_pre_relax = np.max(forces_pre_relax)

    relax_structure(
        PbTeWithCalc,
        max_force=0.05,
        max_n_steps=1000,
        minimizer=minimizer,
        constant_cell=constant_cell,
        constant_volume=constant_volume,
    )

    max_post_relax = np.max(PbTeWithCalc.get_forces())
    assert max_post_relax < 0.05
    assert max_post_relax < max_pre_relax
    assert not np.allclose(PbTeWithCalc.get_positions(),
                           pos_pre_relax)  # Make sure that the atoms moved
    assert np.isclose(max_post_relax, max_force, atol=1e-12, rtol=1e-5)
    assert np.isclose(PbTeWithCalc.get_volume(), volume, atol=1e-12, rtol=1e-5)
    assert np.allclose(PbTeWithCalc.get_cell(), cell, atol=1e-12, rtol=1e-5)


@pytest.mark.parametrize('with_calculator, minimizer, error',
                         [
                             (False, 'fire', 'Structure has no attached calculator object'),
                             (True, 'invalid', 'Unknown minimizer: invalid'),
                          ]
                         )
def test_no_calculator(PbTe, PbTeWithCalc, with_calculator, minimizer, error):
    structure = PbTeWithCalc if with_calculator else PbTe
    with pytest.raises(ValueError) as e:
        relax_structure(structure, minimizer=minimizer)
    assert error in str(e)
