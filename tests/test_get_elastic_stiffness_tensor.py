import numpy as np
import os
import pytest
from ase.build import bulk
from ase.calculators.emt import EMT
from calorine.calculators import CPUNEP
from calorine.tools import get_elastic_stiffness_tensor


@pytest.fixture
def fcc_aluminum():
    structure = bulk('Al', 'fcc', a=4.05, cubic=True)
    structure.calc = EMT()
    return structure


@pytest.fixture
def diamond_aluminum():
    structure = bulk('Al', 'diamond', a=7.2, cubic=True)
    structure.calc = EMT()
    return structure


@pytest.fixture
def rocksalt():
    structure = bulk('PbTe', 'rocksalt', a=4.18, cubic=True)
    model_path = os.path.join('tests', 'nep_models', 'nep3_v3.3.1_PbTe_Fan22.txt')
    structure.calc = CPUNEP(model_path)
    return structure


@pytest.fixture
def zincblende():
    structure = bulk('PbTe', 'zincblende', a=9.9, cubic=True)
    model_path = os.path.join('tests', 'nep_models', 'nep3_v3.3.1_PbTe_Fan22.txt')
    structure.calc = CPUNEP(model_path)
    return structure


def get_reference_data(structure_name, clamped, epsilon):
    if structure_name == 'fcc_aluminum' and not clamped and epsilon == 0.01:
        return np.array(
            [[3.593e+02, 2.959e+01, 2.959e+01, 2.221e-01, -9.224e-01, -7.156e-01],
             [2.959e+01, 3.593e+02, 2.959e+01, -7.156e-01, 2.221e-01, -9.224e-01],
             [2.959e+01, 2.959e+01, 3.593e+02, -9.224e-01, -7.156e-01, 2.221e-01],
             [2.221e-01, -7.156e-01, -9.224e-01, 3.177e+01, -4.965e-03, -4.965e-03],
             [-9.224e-01, 2.221e-01, -7.156e-01, -4.965e-03, 3.177e+01, -4.965e-03],
             [-7.156e-01, -9.224e-01, 2.221e-01, -4.965e-03, -4.965e-03, 3.177e+01]])
    if structure_name == 'diamond_aluminum' and not clamped and epsilon == 0.01:
        return np.array(
            [[752.199,   3.305,   3.305, -190.501, -174.213, -174.878],
             [3.305, 752.199,   3.305, -174.878, -190.501, -174.213],
             [3.305,   3.305, 752.199, -174.213, -174.878, -190.501],
             [-190.501, -174.878, -174.213, -2577.73, 1472.48, 1472.48],
             [-174.213, -190.501, -174.878, 1472.48, -2577.73, 1472.48],
             [-174.878, -174.213, -190.501, 1472.48, 1472.48, -2577.73]])
    if structure_name == 'diamond_aluminum' and clamped and epsilon == 0.01:
        return np.array(
            [[7.522e+02, 3.305e+00, 3.305e+00, -7.522e-02, 2.991e-02, -3.165e-02],
             [3.305e+00, 7.522e+02, 3.305e+00, -3.165e-02, -7.522e-02, 2.991e-02],
             [3.305e+00, 3.305e+00, 7.522e+02, 2.991e-02, -3.165e-02, -7.522e-02],
             [-7.522e-02, -3.165e-02, 2.991e-02, -3.021e+00, 1.304e-03, 1.304e-03],
             [2.991e-02, -7.522e-02, -3.165e-02, 1.304e-03, -3.021e+00, 1.304e-03],
             [-3.165e-02, 2.991e-02, -7.522e-02, 1.304e-03, 1.304e-03, -3.021e+00]])
    if structure_name == 'rocksalt' and not clamped and epsilon == 0.01:
        return np.array(
            [[1.0037641e+03, 1.4510336e+02, 1.4510336e+02,
              3.1168043e+00, 3.0261132e+00, -2.1803301e-01],
             [1.4510336e+02, 1.0037641e+03, 1.4510336e+02,
              -2.1803301e-01, 3.1168043e+00, 3.0261132e+00],
             [1.4510336e+02, 1.4510336e+02, 1.0037641e+03,
              3.0261132e+00, -2.1803300e-01, 3.1168043e+00],
             [3.1168043e+00, -2.1803301e-01, 3.0261132e+00,
              7.0731884e+01, -4.8021701e-02, -4.8021701e-02],
             [3.0261132e+00, 3.1168043e+00, -2.1803300e-01,
              -4.8021701e-02, 7.0731884e+01, -4.8021701e-02],
             [-2.1803301e-01, 3.0261132e+00, 3.1168043e+00,
              -4.8021701e-02, -4.8021701e-02, 7.0731884e+01]])
    if structure_name == 'rocksalt' and not clamped and epsilon == 0.02:
        return np.array(
            [[9.3889696e+02, 1.7251195e+02, 1.7251195e+02,
              6.1551790e+00, 5.0429203e+00, -7.3715321e-01],
             [1.7251195e+02, 9.3889696e+02, 1.7251195e+02,
              -7.3715321e-01, 6.1551790e+00, 5.0429203e+00],
             [1.7251195e+02, 1.7251195e+02, 9.3889696e+02,
              5.0429203e+00, -7.3715321e-01, 6.1551790e+00],
             [6.1551790e+00, -7.3715321e-01, 5.0429203e+00,
              7.0640932e+01, -1.9046936e-01, -1.9046936e-01],
             [5.0429203e+00, 6.1551790e+00, -7.3715321e-01,
              -1.9046936e-01, 7.0640932e+01, -1.9046936e-01],
             [-7.3715321e-01, 5.0429203e+00, 6.1551790e+00,
              -1.9046936e-01, -1.9046936e-01, 7.0640932e+01]])
    if structure_name == 'zincblende' and not clamped and epsilon == 0.01:
        return np.array(
            [[2.2968384e+00, 9.5042919e-01, 9.5042919e-01,
              -1.8284687e-03, -5.6175396e-03, -3.0422178e-03],
             [9.5042919e-01, 2.2968384e+00, 9.5042919e-01,
              -3.0422178e-03, -1.8284686e-03, -5.6175396e-03],
             [9.5042919e-01, 9.5042919e-01, 2.2968384e+00,
              -5.6175396e-03, -3.0422178e-03, -1.8284686e-03],
             [-1.8284687e-03, -3.0422178e-03, -5.6175396e-03,
              4.3982131e-01, 3.4204004e-04, 3.4204005e-04],
             [-5.6175396e-03, -1.8284686e-03, -3.0422178e-03,
              3.4204004e-04, 4.3982131e-01, 3.4204005e-04],
             [-3.0422178e-03, -5.6175396e-03, -1.8284686e-03,
              3.4204005e-04, 3.4204005e-04, 4.3982131e-01]])
    if structure_name == 'zincblende' and not clamped and epsilon == 0.02:
        return np.array(
            [[1.7866943e+00, 8.7916021e-01, 8.7916021e-01,
              -2.3920300e-03, -9.9388292e-03, -4.9992044e-03],
             [8.7916021e-01, 1.7866943e+00, 8.7916021e-01,
              -4.9992044e-03, -2.3920300e-03, -9.9388292e-03],
             [8.7916021e-01, 8.7916021e-01, 1.7866943e+00,
              -9.9388292e-03, -4.9992044e-03, -2.3920300e-03],
             [-2.3920300e-03, -4.9992044e-03, -9.9388292e-03,
              4.4081199e-01, 1.3621959e-03, 1.3621959e-03],
             [-9.9388292e-03, -2.3920300e-03, -4.9992044e-03,
              1.3621959e-03, 4.4081199e-01, 1.3621959e-03],
             [-4.9992044e-03, -9.9388292e-03, -2.3920300e-03,
              1.3621959e-03, 1.3621959e-03, 4.4081199e-01]])
    if structure_name == 'zincblende' and clamped and epsilon == 0.01:
        return np.array(
            [[2.2968384e+00, 9.5042919e-01, 9.5042919e-01,
              -5.0237550e-03, -9.5922870e-03, -4.0010641e-03],
             [9.5042919e-01, 2.2968384e+00, 9.5042919e-01,
              -4.0010641e-03, -5.0237550e-03, -9.5922870e-03],
             [9.5042919e-01, 9.5042919e-01, 2.2968384e+00,
              -9.5922870e-03, -4.0010641e-03, -5.0237550e-03],
             [-5.0237550e-03, -4.0010641e-03, -9.5922870e-03,
              7.4797340e-01, -1.2642715e-04, -1.2642715e-04],
             [-9.5922870e-03, -5.0237550e-03, -4.0010641e-03,
              -1.2642715e-04, 7.4797340e-01, -1.2642715e-04],
             [-4.0010641e-03, -9.5922870e-03, -5.0237550e-03,
              -1.2642715e-04, -1.2642715e-04, 7.4797340e-01]])
    if structure_name == 'zincblende' and clamped and epsilon == 0.02:
        return np.array(
            [[1.7866943e+00, 8.7916021e-01, 8.7916021e-01,
              -9.8656671e-03, -1.8923747e-02, -8.1526914e-03],
             [8.7916021e-01, 1.7866943e+00, 8.7916021e-01,
              -8.1526914e-03, -9.8656671e-03, -1.8923747e-02],
             [8.7916021e-01, 8.7916021e-01, 1.7866943e+00,
              -1.8923747e-02, -8.1526914e-03, -9.8656671e-03],
             [-9.8656671e-03, -8.1526914e-03, -1.8923747e-02,
              7.4785491e-01, -5.0518551e-04, -5.0518551e-04],
             [-1.8923747e-02, -9.8656671e-03, -8.1526914e-03,
              -5.0518551e-04, 7.4785491e-01, -5.0518551e-04],
             [-8.1526914e-03, -1.8923747e-02, -9.8656671e-03,
              -5.0518551e-04, -5.0518551e-04, 7.4785491e-01]])


@pytest.mark.parametrize('structure_name,clamped,epsilon',
                         [
                             ('fcc_aluminum', False, 0.01),
                             ('diamond_aluminum', False, 0.01),
                             ('diamond_aluminum', True, 0.01),
                             ('rocksalt', False, 0.01),
                             ('rocksalt', False, 0.02),
                             ('zincblende', False, 0.01),
                             ('zincblende', False, 0.02),
                             ('zincblende', True, 0.01),
                             ('zincblende', True, 0.02),
                         ])
def test_get_elastic_stiffness_tensor(structure_name, clamped, epsilon, request):
    structure = request.getfixturevalue(structure_name)
    C_ref = get_reference_data(structure_name, clamped, epsilon)

    # Compute the elastic stiffness tensor
    C = get_elastic_stiffness_tensor(structure, clamped=clamped, epsilon=epsilon)

    # Compare to reference values
    assert np.isclose(C, C_ref, atol=0.1).all()

    # Check that the shape of the tensor is correct
    assert C.shape
