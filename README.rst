calorine
========

**calorine** is a Python package for running and analyzing molecular dynamics (MD) simulations via `GPUMD <https://gpumd.zheyongfan.org/>`_.
It also provides functionality for constructing and sampling neuroevolution potentials (NEPs) via `GPUMD <https://gpumd.zheyongfan.org/>`_.
The full documentation can be found at https://calorine.materialsmodeling.org/.
